/* 
multiple line comment
*/
--reto select
select first_name, last_name, email from customer


--clase order by 
select * from payment order by customer_id, amount desc

--reto order by
select first_name, last_name, email from customer order by last_name desc, first_name desc
select first_name, last_name, email from customer order by 2 desc, 1 desc


--reto select distinct
select distinct amount from payment order by amount desc

--day 1 challenge
select distinct district from address 
select rental_date from rental order by rental_date desc limit 1
select count (*) from film
select count (distinct last_name) from customer

--reto where
select count (*) from payment where customer_id = 100
select first_name, last_name from customer where first_name = 'ERICA'


--reto where operators
select count (*) from rental where return_date is null
select payment_id, amount from payment where amount <= 2 order by amount desc


--reto where AND/OR
select * from payment where (customer_id = 322 or customer_id = 346 or customer_id = 354)
and (amount <2 or amount >10) order by customer_id, amount desc

--clase between and
select * from rental where rental_date between '2005-05-24' and '2005-05-26 23:59'
order by rental_date desc

--reto between and
select count(*) from payment where payment_date between '2020-01-26' and '2020-01-27 23:59' and amount between 1.99 and 3.99


--reto in / not in
select * from payment where customer_id in (12, 25, 67, 93, 124, 234) and amount in (4.99, 7.99, 9.99) and payment_date between '2020-01-01' and '2020-01-31 23:59' order by payment_date

--clase like ilike (not like, not ilike)
select * from film where description like '%Drama%'
and title like 'T%'

--reto like
select count (*) from film where description like '%Documentary%'

select * from customer where first_name like '___'
and (last_name like '%X' or last_name like '%Y')


-- aliases ...renombrar el output a un nuevo nombre de columna
select payment_id as pagoID from payment


--day 2 challenge
select count (*) as no_of_movies from film where description like '%Saga%'
and (title like 'A%' or title like '%R')


select * from customer where first_name like '_A%ER%' order by last_name desc

select * from payment where (amount = 0 or amount between 3.99 and 7.99) and payment_date between '2020-05-01' and '2020-05-02'


--agrregate funmctins

--prueba 1
select sum(amount) as amounts, sum(payment_id) as payment_ids, round(avg(amount),3) as average from payment

--reto aggregate function
select min(replacement_cost), max(replacement_cost), round(avg(replacement_cost),2) as average , sum(replacement_cost) from film


--clase group by 
select customer_id, sum(amount) as suma from payment where customer_id > 3 group by customer_id order by suma desc

--reto group by
select staff_id, sum(amount) ,count(*) from payment group by staff_id

select staff_id, sum(amount) ,count(*) from payment where amount > 0 group by staff_id

select amount from payment where amount = 0

-- group by multiple columns
select staff_id, customer_id, sum(amount) ,count(*) from payment group by staff_id, customer_id order by 4 desc

--reto group by multiple columns
select staff_id, date(payment_date) as fecha, sum(amount) from payment group by staff_id, fecha order by 3 desc

select staff_id, date(payment_date) as fecha, sum(amount) from payment where amount != 0 group by staff_id, fecha order by 3 desc

--clase having .. filtrar el output aun mas
select staff_id, date(payment_date) as fecha, sum(amount) from payment where amount != 0 group by staff_id, fecha having sum(amount) > 1000  order by 3 desc
select staff_id, date(payment_date) as fecha, sum(amount) from payment where amount != 0 group by staff_id, fecha having sum(amount) > 1000 or sum(amount) < 500  order by 3 desc


--reto having
select round(avg(amount),2) as average, customer_id, date(payment_date) as fecha , count(*) as num from payment where date(payment_date) between '2020-04-28' and '2020-04-30 23:59' group by customer_id, fecha having count(*) > 1 order by 1 desc 


--clase function length lower upper
select upper(email) from customer
select upper(email) as email, lower(email) as email2, length(email) from customer where length(email) < 30

--reto function length lower upper
select lower(first_name) as name, lower(last_name) as lastname, lower(email) as email from customer where length(first_name) > 10 or length(last_name) > 10

--reto lelft and right
select right(lower(email),5) from customer
select left(right(lower(email),4),1) from customer

--clase concatenate
select left(first_name,1) || left(last_name, 1) from customer

--reto concatenate
select email , left(email,1)||'***'||right(email, 19) from customer

--clase position
select position('@' in email) from customer

--reto position
select email, last_name, last_name||', '||left(email, position(last_name in email)-2) from customer

--clase substring 
select substring(email from 2 for 3), email from customer
select substring(email from position('.'in email)+1  for length(last_name)), email from customer

--reto substring
select left(email,1) || '***' || substring(email from position('.' in email) for 2) || '***' || substring(email from position('@' in email) for length(email))  from customer
select '***'||substring(email from (position('.' in email)-1) for 3) || '***' || substring(email from position('@' in email) for length(email)) from customer

--reto extract
select extract(month from payment_date) as mes, sum(amount)  from payment group by mes order by 2 desc
select extract(DOW from payment_date) as dia, sum(amount)  from payment group by dia order by 2 desc
select extract(week from payment_date) as semana, sum(amount) ,customer_id   from payment group by semana, customer_id order by 2 desc

--reto to char
select sum(amount),
to_char(payment_date,'Dy DD/MM/YYYY') as fecha from payment group by fecha order by 1 desc
select sum(amount),
to_char(payment_date,'Mon,YYYY') as fecha from payment group by fecha order by 1 desc
select sum(amount),
to_char(payment_date,'dy, HH12:MI') as fecha from payment group by fecha order by 1 desc   

--clase timestamp and intervals
select current_timestamp
select current_timestamp,rental_date , current_timestamp - rental_date from rental

--we can use this intervals in extract and to-char
select current_timestamp,extract(day from return_date - rental_date ) , return_date from rental


--reto timestamp and intervals
select customer_id, return_date - rental_date as duracion from rental where customer_id = 35 
select customer_id, avg(return_date - rental_date) as duracion from rental group by customer_id order by customer_id , duracion desc

--clase funciones mate y operadores
select film_id, rental_rate as old_rental_rate, ceiling(rental_rate*1.1) as new_rental_rate from film

--reto funciones mate y operadores
select film_id, rental_rate/replacement_cost as relation, round(rental_rate/replacement_cost *100,2) || '%' as porcentaje 
from film where rental_rate < replacement_cost * 0.04 group by film_id order by 3 asc

--clase case
select amount, case
when amount < 2 then 'low amount'
when amount < 5 then 'medium amount'
else 'high amount'
end from payment

select
to_char(book_date, 'Dy'),
to_char(book_date, 'Mon'),
case
when to_char(book_date, 'Dy') = 'Mon' then 'Monday special'
when to_char(book_date, 'Mon') = 'Jul' then 'July special'
else 'no special'
end from bookings

--reto CASE ;.. recordar que se puede hacer group by el case

--#1
select
count(*),
case
when total_amount < 20000 then 'Low Price Ticket'
when total_amount between 20000 and 150000  then 'Mid Price Ticket'
when total_amount >= 150000 then 'High Price Ticket'
else 'no info'
end as typeOfPrice
from bookings group by typeOfPrice



-- #2

select
count(*),
case
when to_char(actual_departure, 'MM') in ('12','01','02') then 'Winter' 
when to_char(actual_departure, 'MM') in ('03','04','05') then 'Spring' 
when to_char(actual_departure, 'MM') in ('06','07','08') then 'Summer' 
when to_char(actual_departure, 'MM') in ('09','10','11') then 'Fall'
else 'no time'
end as vuelos 
from flights group by vuelos


-- #3
SELECT
title,
CASE
WHEN rating IN ('PG','PG-13') OR length > 210 THEN 'Great rating or long (tier 1)'
WHEN description LIKE '%Drama%' AND length>90 THEN 'Long drama (tier 2)'
WHEN description LIKE '%Drama%' THEN 'Short drama (tier 3)'
WHEN rental_rate<1 THEN 'Very cheap (tier 4)'
END as tier_list
FROM film
WHERE 
CASE
WHEN rating IN ('PG','PG-13') OR length > 210 THEN 'Great rating or long (tier 1)'
WHEN description LIKE '%Drama%' AND length>90 THEN 'Long drama (tier 2)'
WHEN description LIKE '%Drama%' THEN 'Short drama (tier 3)'
WHEN rental_rate<1 THEN 'Very cheap (tier 4)'
END is not null

--clase case with sum
select
rating,
sum(case
when rating in ('PG', 'G') then 1
else 0
end) as no_of_ratingswithGorPG
from film group by rating

--clase coalesce
select coalesce(actual_arrival-scheduled_arrival, '00:00:00') from flights


--clase cast
select coalesce(cast(actual_arrival-scheduled_arrival as VARCHAR), 'not time') from flights

--reto cast y coalesce
select rental_date,
coalesce(cast(return_date as VARCHAR),'no date')
from rental order by rental_date desc

--clase replace
select cast(replace(passenger_id,' ','7777') as BIGINT) from tickets

--clase inner join
select * from payment inner join customer on payment.customer_id = customer.customer_id
select payment.* , first_name, last_name from payment inner join customer on payment.customer_id = customer.customer_id


--reto inner join
select * from seats
select * from boarding_passes

select
fare_conditions,
count(*)
from seats inner join boarding_passes on seats.seat_no = boarding_passes.seat_no group by fare_conditions


--clase full outer join
select * from tickets
select * from boarding_passes

select * from tickets full outer join boarding_passes on tickets.ticket_no = boarding_passes.ticket_no

--reto full outer join with where
select * from tickets full outer join boarding_passes on tickets.ticket_no = boarding_passes.ticket_no where boarding_passes.ticket_no is null

--clase left join  (left outer join)
select * from aircrafts_data as aviones
left join flights as vuelos on aviones.aircraft_code = vuelos.aircraft_code
where vuelos.flight_no is null

-- reto left join
select a.seat_no, count(*) from seats a
full outer join boarding_passes b on a.seat_no = b.seat_no group by a.seat_no order by 2 


-- clase right join
select a.seat_no, count(*) from seats a
full right join boarding_passes b on a.seat_no = b.seat_no group by a.seat_no order by 2 


--retos joins
select customer.first_name, customer.last_name, address.phone from customer 
left join address on customer.address_id = address.address_id
where address.district = 'Texas'

select customer.first_name, customer.last_name, address.address from customer 
right join address on customer.address_id = address.address_id
where customer.first_name is null


--clase multiple join conditions
select * from ticket_flights
select * from boarding_passes

select avg(amount), seat_no from ticket_flights as vuelos
inner join boarding_passes as pases
on vuelos.ticket_no = pases.ticket_no
and vuelos.flight_id = pases.flight_id group by seat_no



--reto multiple table with joins  
select first_name, last_name, email, country from customer
inner join address on customer.address_id = address.address_id
inner join city on address.city_id = city.city_id
inner join country on city.country_id = country.country_id
and country = 'Brazil'


--clase subqueries
select avg(amount) from payment
select * from payment where amount > 4.2006673312979002

select * from payment where amount > (select avg(amount) from payment)

--reto subqueries in where
select * from film where length > (select round(avg(length),2) as prom from film) order by 2
select * from film where film_id in (select film_id from inventory where store_id = 2 group by film_id  having count(*) > 3)


--reto subqueries in from
select avg(amount_perday)from (select 
sum (amount) as amount_perday, 
date(payment_date)
from payment
group by date(payment_date)) as asd

--reto subqueries in select
select * , (select max(amount)from payment )-amount as difference from payment

--clase correlated subqueries
select * from payment p1
where amount = (select max(amount) from payment p2 where p1.customer_id = p2.customer_id)

--reto correlated subqueries in where
select title, film_id, replacement_cost, rating
from film as f1
where replacement_cost = (select min(replacement_cost) from film f2 where f1.rating = f2.rating)

select title, film_id, length, rating
from film as f1
where length = (select max(length) from film f2 where f1.rating = f2.rating)

--correlated in select
select *, (select max(amount) from payment p2 where p1.customer_id= p2.customer_id)from payment p1 order by customer_id
 
 
--reto general

select  distinct(replacement_cost) from film order by 1

select   
case
when replacement_cost < 19.99 then 'Low'
when replacement_cost < 24.99 then 'medium'
else 'High' 
end as caso, count(*) from film group by caso 
 
-- crear tablas
create table director (
director_id serial primary key,
director_account_name varchar(20) unique,
first_name varchar(50),
last_name varchar(50) default 'Not specified',
date_of_birth date,
address_id int references address(address_id)	
)


create table online_sales (
transaction_id serial primary key,
customer_id int references customer(customer_id),
film_id int references film(film_id),
amount int not null,
promotion_code varchar(10) default 'None'
)

insert into online_sales values (1, 269, 13, 10.99, 'Bundle2022')


create table songs(
song_id serial primary key,
song_name varchar(30) not null,
genre varchar(30) default 'Not defined'
price numeric(4,2) check (price >=1.99),
release_date date constraint date_check check(release_date between '01-01-1950' and current_date)	
	

)


--update clase
update customer set last_name= 'Brown'
where customer_id = 1

update customer set email=lower(email)

--reto update
update film set rental_rate=1.99 where rental_rate=0.99

--reto delete
delete from payment where payment_id in (17064,17067) returning *

--reto views 
CREATE VIEW films_category
AS
SELECT 
title,
name,
length
FROM film f
LEFT JOIN film_category fc
ON f.film_id=fc.film_id
LEFT JOIN category c
ON c.category_id=fc.category_id
WHERE name IN ('Action','Comedy')
ORDER BY length DESC


--clase windows functions 

select *, 
sum(amount) over(partition by customer_id)from payment order by 1
 
select *, 
sum(amount) over(partition by customer_id, staff_id)from payment order by 1


--reto windows functions over partition by
select film.film_id, 
title,
length,
category.name as category,
avg(length) over (partition by category)
from film inner join film_category on film.film_id = film_category.film_id
inner join category on film_category.category_id = category.category_id

select *, count(*) over (partition by amount, customer_id)from payment

--clase windows functions over order by
select *, 
sum(amount) over (order by payment_date) from payment

select *, 
sum(amount) over (partition by customer_id order by payment_date) from payment

--reto windows functions over order by

select flight_id,
departure_airport,
sum(actual_arrival-scheduled_arrival) over (order by flight_id) from flights

--clase rank
select
f.title,
c.name,
f.length
rank() over (order by length) from film
left join film_category fc on f.film_id = 

--reto rank
select
name,
country,
count(*),
rank() over (partition by country order by count(*) desc )
from customer_list
left join payment on id=customer_id group by name, country


--reto lead lag
select
sum(amount),
date(payment_date),
lag(sum(amount)) over (order by date(payment_date)),
sum(amount) -lag(sum(amount)) over (order by date(payment_date))
from payment
group by date(payment_date)


--clase grouping sets
select staff_id,
to_char(payment_date, 'Month') as mes,
sum(amount) from payment group by grouping sets ((staff_id),(mes),(staff_id,mes))

-- reto grouping sets
select
first_name,
last_name,
staff_id,
sum(amount)
from customer c left join payment p on c.customer_id = p.customer_id group by grouping sets((first_name,last_name),(first_name,last_name,staff_id))


--clase rollup 
select
'Q'|| to_char(payment_date,'Q') as quarter,
extract(month from payment_date) as month,
date(payment_date),
sum(amount) from payment
group by rollup (
'Q'|| to_char(payment_date,'Q'),
extract(month from payment_date),
date(payment_date)
)


--reto group by rollup
select
extract(quarter from book_date) as quarter,
extract(month from book_date) as month,
to_char(book_date,'w') as week_in_month,
date(book_date),
sum(total_amount)
from bookings
group by rollup (
extract(quarter from book_date) ,
extract(month from book_date) ,
to_char(book_date,'w') ,
date(book_date)
) 

--clase group by cube
select customer_id,
staff_id,
date(payment_date),
sum(amount)
from payment
group by cube(customer_id,
staff_id,
date(payment_date)) order by 1, 2,3

--reto group by cube
SELECT 
p.customer_id,
DATE(payment_date),
title,
SUM(amount) as total
FROM payment p
LEFT JOIN rental r
ON r.rental_id=p.rental_id
LEFT JOIN inventory i
ON i.inventory_id=r.inventory_id
LEFT JOIN film f
ON f.film_id=i.film_id
GROUP BY 
CUBE(
p.customer_id,
DATE(payment_date),
title
)
ORDER BY 1,2,3



--reto self joins
select
f1.title ,
f2.title,
f2.length
from film f1
left join film f2
on f1.length = f2.length
order by length desc

select
f1.title ,
f2.title,
f2.length
from film f1
left join film f2
on f1.length = f2.length
and
f1.title <> f2.title
order by length desc



--transcations


CREATE TABLE acc_balance (
    id SERIAL PRIMARY KEY,
    first_name TEXT NOT NULL,
	last_name TEXT NOT NULL,
    amount DEC(9,2) NOT NULL    
);

INSERT INTO acc_balance
VALUES 
(1,'Tim','Brown',2500),
(2,'Sandra','Miller',1600)

SELECT * FROM acc_balance;

begin;
update acc_balance
set amount = amount -100
where id = 1;
update acc_balance
set amount = amount -100
where id = 2;
commit;

--reto privileges user management
-- Create user
CREATE USER mia
WITH PASSWORD 'mia123';
 
-- Create role
CREATE ROLE analyst_emp;
 
-- Grant privileges
GRANT SELECT
ON ALL TABLES IN SCHEMA public
TO analyst_emp;
 
GRANT INSERT,UPDATE
ON employees
TO analyst_emp;
 
-- Add permission to create databases
ALTER ROLE analyst_emp CREATEDB;
 
-- Assign role to user
GRANT analyst_emp TO mia;

